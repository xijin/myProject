﻿using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Ruanmou.Interface;
//using Ruanmou.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ruanmou.Project
{
    /// <summary>
    /// 1 IOC(DI)介绍
    /// 2 Unity容器使用
    /// 3 两种Unity的AOP方式
    /// 
    /// DIP依赖倒置原则：上层模块不应该依赖于低层模块，二者应该通过抽象来依赖，  依赖抽象，而不是依赖细节
    /// IOC控制反转：程序上层对下层的依赖，转移到第三方的容器来装配
    ///              其实是我们程序设计的目标，实现方式包含了依赖注入(DI)和依赖查找
    /// 依赖注入DI是手段，分三种
    /// 构造函数注入  属性注入   方法注入
    /// 
    /// 1 EF进阶延迟加载、导航属性、多种事务
    /// 
    /// 
    /// 1 EF分层封装数据访问
    /// 2 EF和IOC结合
    /// 3 多线程任务分配(插播)
    /// 4 作业部署
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("欢迎来到.net高级班vip课程，今天是IOC");
                {
                    //{
                    //    AndroidPhone phone = new AndroidPhone();
                    //}
                    //{
                    //    IPhone phone = new AndroidPhone();
                    //}
                    //{
                    //    IPhone phone = ObjectFactory.CreatePhone();
                    //}

                    //IOCTest.Show();
                }
                {
                    //EFTest.Show();
                }
                {
                    //EFAdvancedTest.Show();
                }

                {
                    //EFIOCTest.Show();
                }
                {
                    //两个多线程问题：
                    {
                        //1 1400多个类别需要数据抓取，各自耗时差别特别大，自定义N个线程，怎么样尽量均匀的分配
                        List<int> list = new List<int>();
                        for (int i = 0; i < 1400; i++)
                        {
                            list.Add(i);
                        }
                        int threadNumber = 13;

                        ParallelOptions parallelOptions = new ParallelOptions();
                        parallelOptions.MaxDegreeOfParallelism = threadNumber;
                        Parallel.ForEach<int>(list, parallelOptions, i =>
                        {
                            Console.WriteLine($"线程id{Thread.CurrentThread.ManagedThreadId}&{i}在工作");
                            Thread.Sleep(i);
                        });
                    }
                    {
                        List<int> list = new List<int>();
                        for (int i = 0; i < 1400; i++)
                        {
                            list.Add(i);
                        }
                        int threadNumber = 13;

                        List<Task> taskList = new List<Task>();
                        TaskFactory taskFactory = new TaskFactory();
                        foreach (var item in list)
                        {
                            taskList.Add(taskFactory.StartNew(() =>
                            {
                                Console.WriteLine($"线程id{Thread.CurrentThread.ManagedThreadId}&{item}在工作");
                                Thread.Sleep(item);
                            }));
                            if (taskList.Count >= threadNumber)
                            {
                                Task.WaitAny(taskList.ToArray());
                                taskList = taskList.Where(t => t.Status == TaskStatus.Running).ToList();//小新
                            }
                        }

                        Task.WaitAll(taskList.ToArray());
                    }



                    //2 30张表 1000w数据，怎么样合理的自定义个数的线程，去数据库查询完数据后写入到不同的硬盘索引文件，让每个线程的任务量尽量均等

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.Read();
        }
    }
}
