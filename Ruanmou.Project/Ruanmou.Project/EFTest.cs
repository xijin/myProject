﻿using Ruanmou.EF.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ruanmou.Project
{
    public class EFTest
    {
        public static void Show()
        {
            using (JDDbContext dbContext = new JDDbContext())
            {
                dbContext.Database.Log += c => Console.WriteLine(c);
                {   //实体查询   延迟查询
                    User user2 = dbContext.User.Find(2);

                    var list2 = dbContext.User.Where(u => u.Id < 10);//延迟查询

                    list2 = list2.Where(u => u.Account.Length > 3);
                    //dbContext.User空集合  只有表达式树和解析方法
                    //真的用数据的时候，才去执行的
                    foreach (var item in list2)//迭代的时候查询
                    {

                    }
                    var list = dbContext.User.Where(u => u.Id < 3).ToList(); //立即查询
                }
                {
                    User userNew = new User()
                    {
                        Account = "Admin",
                        State = 0,
                        CompanyId = 4,
                        CompanyName = "万达集团",
                        CreateTime = DateTime.Now,
                        CreatorId = 1,
                        Email = "57265177@qq.com",
                        LastLoginTime = null,
                        LastModifierId = 0,
                        LastModifyTime = DateTime.Now,
                        Mobile = "18664876671",
                        Name = "yoyo",
                        Password = "12356789",
                        UserType = 1
                    };
                    dbContext.User.Add(userNew);
                    dbContext.SaveChanges();//

                    userNew.Name = "安德鲁";
                    dbContext.SaveChanges();

                    dbContext.User.Remove(userNew);
                    dbContext.SaveChanges();
                }

                {
                    var list = dbContext.User.Where(u => new int[] { 1, 2, 3, 5, 7, 8, 9, 10, 11, 12, 14 }.Contains(u.Id));

                    list = list.Where(v => v.Id < 5);
                    list = list.OrderBy(v => v.Id);

                    foreach (var user in list)
                    {
                        Console.WriteLine(user.Name);
                    }
                }
                {
                    var list = from u in dbContext.User
                               where new int[] { 1, 2, 3, 5, 7, 8, 9, 10, 11, 12, 14 }.Contains(u.Id)
                               select u;

                    foreach (var user in list)
                    {
                        Console.WriteLine(user.Name);
                    }
                }
                {
                    var list = dbContext.User.Where(u => new int[] { 1, 2, 3, 5, 7, 8, 9, 10, 11, 12, 14 }.Contains(u.Id))
                                              .OrderBy(u => u.Id)
                                              .Select(u => new
                                              {
                                                  Account = u.Account,
                                                  Pwd = u.Password
                                              }).Skip(3).Take(5);
                    foreach (var user in list)
                    {
                        Console.WriteLine(user.Pwd);
                    }
                }
                {
                    var list = (from u in dbContext.User
                                where new int[] { 1, 2, 3, 5, 7, 8, 9, 10, 11, 12, 14 }.Contains(u.Id)
                                orderby u.Id
                                select new
                                {
                                    Account = u.Account,
                                    Pwd = u.Password
                                }).Skip(3).Take(5);

                    foreach (var user in list)
                    {
                        Console.WriteLine(user.Account);
                    }
                }

                {
                    var list = dbContext.User.Where(u => u.Name.StartsWith("小") && u.Name.EndsWith("新"))
                                               .Where(u => u.Name.EndsWith("新"))
                                               .Where(u => u.Name.Contains("小新"))
                                               .Where(u => u.Name.Length < 5)
                                               .OrderBy(u => u.Id);

                    foreach (var user in list)
                    {
                        Console.WriteLine(user.Name);
                    }
                }
                {
                    var list = from u in dbContext.User
                               join c in dbContext.Company on u.CompanyId equals c.Id
                               where new int[] { 1, 2, 3, 4, 6, 7, 10 }.Contains(u.Id)
                               select new
                               {
                                   Account = u.Account,
                                   Pwd = u.Password,
                                   CompanyName = c.Name
                               };
                    foreach (var user in list)
                    {
                        Console.WriteLine("{0} {1}", user.Account, user.Pwd);
                    }
                }
                {
                    var list = from u in dbContext.User
                               join c in dbContext.Category on u.CompanyId equals c.Id
                               into ucList
                               from uc in ucList.DefaultIfEmpty()
                               where new int[] { 1, 2, 3, 4, 6, 7, 10 }.Contains(u.Id)
                               select new
                               {
                                   Account = u.Account,
                                   Pwd = u.Password
                               };
                    foreach (var user in list)
                    {
                        Console.WriteLine("{0} {1}", user.Account, user.Pwd);
                    }
                }
                {
                    DbContextTransaction trans = null;
                    try
                    {
                        trans = dbContext.Database.BeginTransaction();
                        string sql = "Update [User] Set Name='小新' WHERE Id=@Id";
                        SqlParameter parameter = new SqlParameter("@Id", 1);
                        dbContext.Database.ExecuteSqlCommand(sql, parameter);
                        trans.Commit();
                    }
                    catch (Exception ex)
                    {
                        if (trans != null)
                            trans.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        trans.Dispose();
                    }
                }
                {
                    DbContextTransaction trans = null;
                    try
                    {
                        trans = dbContext.Database.BeginTransaction();
                        string sql = "SELECT * FROM [User] WHERE Id=@Id";
                        SqlParameter parameter = new SqlParameter("@Id", 1);
                        List<User> userList = dbContext.Database.SqlQuery<User>(sql, parameter).ToList<User>();
                        trans.Commit();
                    }
                    catch (Exception ex)
                    {
                        if (trans != null)
                            trans.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        trans.Dispose();
                    }
                }

            }
        }

    }
}
