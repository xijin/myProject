﻿using Microsoft.Practices.Unity.Configuration;
using Ruanmou.Bussiness.Interface;
using Ruanmou.Bussiness.Service;
using Ruanmou.EF.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace Ruanmou.Project
{
    public class EFIOCTest
    {
        public static void Show()
        {
            //using (JDDbContext context = new JDDbContext())
            //{
            //    User user = context.Set<User>().Find(2);
            //}

            //using (CategoryService service = new CategoryService())
            //using (CommodityService commodityService = new CommodityService())
            //{
            //    //service.Find(1);
            //    Category category = service.Find<Category>(2);
            //    category.CategoryLevel = category.CategoryLevel + 1;
            //    service.Update(category);
            //    //service.Delete(category);

            //    //CategoryService和CommodityService 
            //    //join查询  会报错
            //    //var model = from c in service.DbSet<Category>()
            //    //            join com in commodityService.DbSet<Commodity>()
            //    //            on c.Id equals com.CategoryId
            //    //            where c.Id == 181
            //    //            select c;
            //    //foreach (var item in model)
            //    //{

            //    //}

            //}

            //using (ICategoryCommodityService service = new CategoryCommodityService(new JDDbContext()))
            //{
            //    service.Find<Category>(1);
            //    service.QueryCommodityByCategoryId(2);
            //}

            ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
            fileMap.ExeConfigFilename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "CfgFiles\\Unity.Config");//找配置文件的路径
            Configuration configuration = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
            UnityConfigurationSection section = (UnityConfigurationSection)configuration.GetSection(UnityConfigurationSection.SectionName);

            IUnityContainer container = new UnityContainer();
            section.Configure(container, "ruanmouContainer");

            using (ICategoryCommodityService service = container.Resolve<ICategoryCommodityService>())
            {
                service.Find<Category>(1);
                service.QueryCommodityByCategoryId(2);

                service.QueryPage<Category, int>(c => c.Id < 100 && c.CategoryLevel > 0 && c.Name.Length > 1, 10, 2, c => c.CategoryLevel ?? 1, true);


            }
        }
    }
}
