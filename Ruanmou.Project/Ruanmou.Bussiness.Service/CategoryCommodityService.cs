﻿using Ruanmou.Bussiness.Interface;
using Ruanmou.EF.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ruanmou.Bussiness.Service
{
    /// <summary>
    /// 除了增删改查，还得包含业务逻辑
    /// 1 一个context完成操作更方便
    /// 2 经常会在一起操作的  类别-商品    用户-映射-菜单 
    /// 3 也不是都合在一起的，总有时候会有多个服务一起操作
    /// </summary>
    public class CategoryCommodityService : BaseService, ICategoryCommodityService
    {
        public CategoryCommodityService(DbContext context) : base(context)
        {
        }


        /// <summary>
        /// 比如我给你类别id  帮我找出10条该类别的商品
        /// </summary>
        public void QueryCommodityByCategoryId(int categoryId)
        {
            var model = from c in base.Set<Category>()
                        join com in base.Set<Commodity>()
                        on c.Id equals com.CategoryId
                        where c.Id == 181
                        select c;
            foreach (var item in model)
            {

            }
        }

        public override void Dispose()
        {
            base.Dispose();
            //dispose自己的资源
        }
    }
}
