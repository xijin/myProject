﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;
using Unity.Interception.PolicyInjection.Pipeline;//InterceptionExtension扩展
using Unity.Interception.PolicyInjection.Policies;

namespace Ruanmou.Framework
{
    /// <summary>
    /// 使用EntLib\PIAB Unity 实现动态代理
    /// </summary>

    #region 特性对应的行为
    public class UserHandler : ICallHandler
    {
        public int Order { get; set; }
        public IMethodReturn Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
        {
            Console.WriteLine("参数检测无误");
            IMethodReturn methodReturn = getNext()(input, getNext); //getNext.Invoke().Invoke(input, getNext);
            return methodReturn;
        }
    }

    public class LogHandler : ICallHandler
    {
        public int Order { get; set; }
        public IMethodReturn Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
        {
            Console.WriteLine("日志已记录，Message:{0},Ctime:{1}", "测试LogHandler", DateTime.Now);
            return getNext()(input, getNext);
        }
    }


    public class ExceptionHandler : ICallHandler
    {
        public int Order { get; set; }
        public IMethodReturn Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
        {
            IMethodReturn methodReturn = getNext()(input, getNext);
            if (methodReturn.Exception == null)
            {
                Console.WriteLine("无异常");
            }
            else
            {
                Console.WriteLine("异常:{0}", methodReturn.Exception.Message);
            }
            return methodReturn;
        }
    }

    public class AfterLogHandler : ICallHandler
    {
        public int Order { get; set; }
        public IMethodReturn Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
        {
            IMethodReturn methodReturn = getNext()(input, getNext);
            Console.WriteLine("完成日志，Message:{0},Ctime:{1},计算结果{2}", "测试LogHandler", DateTime.Now, methodReturn.ReturnValue);
            return methodReturn;
        }
    }
    #endregion 特性对应的行为

    #region 特性
    public class UserHandlerAttribute : HandlerAttribute
    {
        public override ICallHandler CreateHandler(IUnityContainer container)
        {
            ICallHandler handler = new UserHandler() { Order = this.Order };
            return handler;
        }
    }

    public class LogHandlerAttribute : HandlerAttribute
    {
        public override ICallHandler CreateHandler(IUnityContainer container)
        {
            return new LogHandler() { Order = this.Order };
        }
    }

    public class ExceptionHandlerAttribute : HandlerAttribute
    {
        public override ICallHandler CreateHandler(IUnityContainer container)
        {
            return new ExceptionHandler() { Order = this.Order };
        }
    }

    public class AfterLogHandlerAttribute : HandlerAttribute
    {
        public override ICallHandler CreateHandler(IUnityContainer container)
        {
            return new AfterLogHandler() { Order = this.Order };
        }
    }
    #endregion 特性

    /*
     TransparentProxyInterceptor：直接在类的方法上进行标记，但是这个类必须继承MarshalByRefObject...不建议用
     VirtualMethod：直接在类的方法上进行标记，但这个方法必须是虚方法（就是方法要带virtual关键字）
     InterfaceInterceptor：在接口的方法上进行标记，这样继承这个接口的类里实现这个接口方法的方法就能被拦截
     */
}
